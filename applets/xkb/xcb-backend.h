
/* xkb-backend.h - Header for xkb backend
 *
 * SPDX-FileCopyrightText: 2023, Konstantin Pugin
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <glib.h>
#include <glib-object.h>

G_DECLARE_FINAL_TYPE(XkbPluginXcbBackend, xkb_plugin_xcb_backend, XKB_PLUGIN, XCB_BACKEND, GObject)

