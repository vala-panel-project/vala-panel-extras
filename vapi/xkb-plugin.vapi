/* xkb-plugin.vapi - VAPI file for XKB plugin
 *
 * SPDX-FileCopyrightText: 2023, Konstantin Pugin
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
namespace XkbPlugin {
    [CCode (cname = "XkbPluginXcbBackend", cheader_filename = "xcb-backend.h", type_id = "xkb_plugin_xcb_backend_get_type()")]
    public class XcbBackend: GLib.Object 
    {
        public bool use_system_layouts {get; set;}
        public bool layout_group_per_window {get; set;}
        public string[] layout_names {get; private set;}
        public string[] layout_short_names {get; private set;}
        public string[] layout_variants {get; private set;}
        public string layout_name {get; private set;}
        public string layout_short_name {get; private set;}
        public string layout_variant {get; private set;}
        public uint32 layout_number {get; private set;}
        public uint32 layouts_count {get; private set;}
        public string rules {get; set;}
        public string model {get; set;}
        public string layout {get; set;}
        public string variant {get; set;}
        public string options {get; set;}
        public signal void layout_changed();
        public signal void keymap_changed();
    
        public XcbBackend();
    
        public void next_layout_group();
        public void prev_layout_group();
        public void set_layout_group(uint32 new_layout_number);
        public void set_keymap();
    }
}