/* gweather-location-entry.h - Location-selecting text entry
 *
 * SPDX-FileCopyrightText: 2008, Red Hat, Inc.
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

[CCode (cprefix = "Gis", gir_namespace = "Gis", gir_version = "3.0", lower_case_cprefix = "gis_")]
namespace Gis {
	[CCode (cheader_filename = "gis-location-entry.h", type_id = "gis_location_entry_get_type ()")]
	public class LocationEntry : Gtk.SearchEntry, Atk.Implementor, Gtk.Buildable, Gtk.CellEditable, Gtk.Editable {
		[CCode (has_construct_function = false, type = "GtkWidget*")]
		public LocationEntry (GWeather.Location top);
		public GWeather.Location? get_location ();
		public bool has_custom_text ();
		public bool set_city (string? city_name, string code);
		public void set_location (GWeather.Location? loc);
		public GWeather.Location location { owned get; set; }
		[NoAccessorMethod]
		public bool show_named_timezones { get; construct; }
		[NoAccessorMethod]
		public GWeather.Location top { construct; }
	}
}